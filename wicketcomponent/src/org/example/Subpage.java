package org.example;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class Subpage extends WebPage {
	public Subpage() {
        add(new Label("oneComponent", "A sub page contributed by bundle."));
    }
}
