package org.example;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Fragment;
import org.example.shared.ComponentProvider;
import org.example.shared.FragmentProvider;
import org.example.shared.PageProvider;

import aQute.bnd.annotation.component.*;

@Component
public class ExtraFragment implements FragmentProvider {
	

	@Override
	public Fragment getFragment() {
		return new Fragment1("Some","",null);
	}
	
	public class Fragment1 extends Fragment {
        public Fragment1(String id, String markupId, MarkupContainer container) {
            super(id, markupId,container);
            add(new Label("label", "Hello, World!"));
        }
    }

	// TODO: class provided by template

}