package org.example;

import aQute.bnd.annotation.component.*;
import org.ops4j.pax.wicket.api.WebApplicationFactory;

@Component (properties= {
		"pax.wicket.applicationname=testapplication",
		"pax.wicket.mountpoint=wicket"
})
public class WicketWebApplicationFactory implements WebApplicationFactory<WicketApplication> {
	
	{
		System.err.println("### INIT " + WicketWebApplicationFactory.class.getName());
	}

    /* (non-Javadoc)
     * @see org.ops4j.pax.wicket.api.WebApplicationFactory#getWebApplicationClass()
     */
    public Class<WicketApplication> getWebApplicationClass() {
        return WicketApplication.class;
    }

    /* (non-Javadoc)
     * @see org.ops4j.pax.wicket.api.WebApplicationFactory#onInstantiation(org.apache.wicket.protocol.http.WebApplication)
     */
    public void onInstantiation(WicketApplication application) {
        //Nothing to do here...
		System.err.println("### INIT " + application );
    }

}
