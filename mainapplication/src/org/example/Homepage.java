package org.example;

import java.util.Arrays;
import java.util.List;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.example.shared.PageProvider;
import org.ops4j.pax.wicket.api.PaxWicketBean;

public class Homepage extends WebPage {

	private static final long       serialVersionUID = 1L;

//	@PaxWicketBean(injectionSource = PaxWicketBean.INJECTION_SOURCE_SERVICE_REGISTRY, name="PageProvider")
//    private transient List<PageProvider> pageProvider;
	
	@PaxWicketBean(injectionSource = PaxWicketBean.INJECTION_SOURCE_SERVICE_REGISTRY)
    private transient PageProvider extra;
    
    public Homepage() {
    	add(new Label("oneComponent", "Welcome"));
    	
    	List<PageProvider> pageProvider = Arrays.asList(extra);
    	
    	ListView<PageProvider> links = new ListView<PageProvider>("links",pageProvider) {
             @Override
             protected void populateItem(ListItem<PageProvider> item) {
                 item.add(new BookmarkablePageLink("link", item.getModelObject().getPageClass()));
             }
         };
         add(links);
    }
}
