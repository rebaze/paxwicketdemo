package org.example.shared;

import org.apache.wicket.markup.html.panel.Fragment;

public interface FragmentProvider {
	Fragment getFragment();
}
