package org.example.shared;

import org.apache.wicket.Component;

public interface ComponentProvider {

    Component getComponent(String id);

}

