package org.example.shared;

import org.apache.wicket.Page;

public interface PageProvider {

    Class<? extends Page> getPageClass();

}
